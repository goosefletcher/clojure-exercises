(ns quipper.core
  (:require [clojure.core.match :refer [match]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.cli :as cli])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file FILE" "Specify an alternate quip file."
                   :default (str (System/getProperty "user.home") "/.quips")]])

(defn print-banner [summary]
  (println "This is the 'quipper' program.")
  (println summary))

(defn file-exists? [file]
  (-> file
      io/as-file
      .exists))

(defn add-quips [file quips]
  (let [current-quips (edn/read-string (slurp file))]
    (spit file (pr-str (concat current-quips quips)))))

(defn count-quips [file]
  (-> file
      slurp
      edn/read-string
      count))

(defn get-quip [file]
  (if (= 0 (count-quips file))
    (println "")
    (-> file
        slurp
        edn/read-string
        rand-nth
        println)))

(defn drop-quips [file]
  (spit file nil))

(defn -main
  "Deal with quips."
  [& args]
  (let [{:keys [options arguments summary errors]} (cli/parse-opts args cli-options)
        {:keys [help file]} options
        [command & quips] arguments]
    (when-not (file-exists? file) (spit file nil))
    (match [help command (empty? quips)]
           [true _ _] (print-banner summary)
           [_ "add" true] (print-banner summary)
           [false "add" false] (add-quips file quips)
           [false "get" _] (get-quip file)
           [false nil true] (get-quip file)
           [false "drop" _] (drop-quips file)
           [false "count" _] (println (count-quips file))
           :else (print-banner summary))))
